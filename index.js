/**
 * Machine string
 * @param {string} input
 *
 * @returns {string}
**/
function machineString(input, separator = '_') {
    return input.toLowerCase()
        .normalize('NFD')
        .replace(/ø/g, 'o')
        .replace(/[æǣǽ]/g, 'ae')
        .replace(/[\u0300-\u036f]/g, '')
        .replace(/[^a-z0-9]/g, ' ')
        .replace(/\s+/g, ' ')
        .trim()
        .replace(/[^a-z0-9]/g, separator);
}

module.exports = machineString;
