var machineString = require('./index.js');
var assert = require('assert');

describe('machine-string', function() {
    it('convert to machine readable string', function () {
        assert.equal(
            'mac_os_x',
            machineString('Mac OS X')
        );
    });

    it('strip extraneous underscores', function () {
        assert.equal(
            'mac_os_x',
            machineString('Mac OS X     ')
        );
    });

    it('strip extraneous prepending underscores', function () {
        assert.equal(
            'mac_os_x',
            machineString('     Mac OS X     ')
        );
    });

    it('strip extraneous mixed underscores', function () {
        assert.equal(
            'mac_os_x',
            machineString('     Mac      OS X     ')
        );
    });
    
    it('allow custom separators', function () {
        assert.equal(
            'mac-os-x',
            machineString('Mac OS X', '-')
        );
    });
    
    it('keep numbers', function () {
        assert.equal(
            'mac-os-x-1-2-3',
            machineString('Mac OS X 1 2 3', '-')
        );
    });
    
    it('transform diacriticals to a', function () {
        assert.equal(
            'a_a_a_a_a_a_a_a_a_a_a_a_a_a_a_a_a_a_a_a_a_a_a_a_a_a_a_a_a',
            machineString('Ä ä Å å À Â Ã Ä Å à á â ã ä å Ā ā Ă ă Ą ą Ǻ ǻ Ȁ ȁ Ȃ ȃ Ȧ ȧ')
        );
    });
    
    it('transform diacriticals to c', function () {
        assert.equal(
            'c_c_c_c_c_c_c_c_c_c',
            machineString('Ç ç Ć ć Ĉ ĉ Ċ ċ Č č')
        );
    });
        
    it('transform diacriticals to e', function () {
        assert.equal(
            'e_e_e_e_e_e_e_e_e_e_e_e_e_e_e_e_e_e_e_e_e_e_e',
            machineString('È É Ê Ë è é ê ë Ē ē Ĕ ĕ Ė ė Ę ę Ě ě Ȅ ȅ Ȇ ȇ Ȩ')
        );
    });
    
    it('transform diacriticals to o', function () {
        assert.equal(
            'o_o_o_o_o_o_o_o_o_o_o_o_o_o_o_o_o_o_o_o_o_o_o_o_o_o_o_o_o_o_o_o_o_o',
            machineString('Ò Ó Ô Õ Ö Ø ò ó ô õ ö ø Ō ō Ŏ ŏ Ő ő Ǫ ǫ Ǭ ǭ Ȫ ȫ Ȭ ȭ Ȯ ȯ Ȱ ȱ Ȍ ȍ Ȏ ȏ')
        );
    });
    
    it('transform diacriticals to i', function () {
        assert.equal(
            'i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i_i',
            machineString('Ì Í Î Ï ì í î ï Ĩ ĩ Ī ī Ĭ ĭ Į į İ Ȉ ȉ Ȋ ȋ')
        );
    });
    
    it('transform diacriticals to u', function () {
        assert.equal(
            'u_u_u_u_u_u_u_u_u_u_u_u_u_u_u_u_u_u_u_u_u_u_u_u_u_u_u_u_u_u_u_u_u_u',
            machineString('Ù Ú Û Ü ù ú û ü Ũ ũ Ū ū Ŭ ŭ Ů ů Ű ű Ų ų Ǔ ǔ Ǖ ǖ Ǘ ǘ Ǚ ǚ Ǜ ǜ Ȕ ȕ Ȗ ȗ')
        );
    });
    
    it('transform diacriticals to g', function () {
        assert.equal(
            'g_g_g_g_g_g_g_g_g_g_g_g',
            machineString('Ĝ ĝ Ğ ğ Ġ ġ Ģ ģ Ǧ ǧ Ǵ ǵ')
        );
    });
    
    it('transform diacriticals to ae', function () {
        assert.equal(
            'ae_ae_ae_ae_ae_ae',
            machineString('Æ æ Ǣ ǣ Ǽ ǽ')
        );
    });
    
    it('transform diacriticals to y', function () {
        assert.equal(
            'y_y_y_y_y_y_y_y',
            machineString('ȳ Ý ý ÿ Ŷ ŷ Ÿ Ȳ')
        );
    });
    
    it('transform diacriticals to n', function () {
        assert.equal(
            'n_n_n_n_n_n_n_n',
            machineString('Ñ ñ Ń ń Ņ ņ Ň ň')
        );
    });
    
    it('transform diacriticals to z', function () {
        assert.equal(
            'z_z_z_z_z',
            machineString('Ź Ż ż Ž ž')
        );
    });
    
    it('transform diacriticals to l', function () {
        assert.equal(
            'l_l_l_l_l_l',
            machineString('Ĺ ĺ Ļ ļ Ľ ľ')
        );
    });
    
    it('transform diacriticals to s', function () {
        assert.equal(
            's_s_s_s_s_s_s_s_s_s',
            machineString('Ś ś Ŝ ŝ Ş ş Š š Ș ș')
        );
    });
    
    it('transform diacriticals to t', function () {
        assert.equal(
            't_t_t_t',
            machineString('Ţ ţ Ť ť')
        );
    });
    
    it('transform diacriticals to w', function () {
        assert.equal(
            'w_w',
            machineString('Ŵ ŵ')
        );
    });
});
