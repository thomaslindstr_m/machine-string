# machine-string

[![build status](https://gitlab.com/thomaslindstr_m/machine-string/badges/master/build.svg)](https://gitlab.com/thomaslindstr_m/machine-string/commits/master)

turn a string into a consistent machine readable string

```
npm install machine-string
```

```javascript
var machineString = require('machine-string');
var input = 'Hello, this is a test 1 2 3 4. Something!';
machineString(input); // > hello_this_is_a_test_1_2_3_4_something

var input = 'Mac OS X';
machineString(input, '-'); // > mac-os-x
```

It also transforms most diacriticals to their safe latin counterpart:

```javascript
machineString('ÄÈÒÙÌÆ'); // > aeouiae
``` 

You can define your own separator in the second argument of the function:

```javascript
machineString('Hello, there!', '-'); // > hello-there
```